<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210327222356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE join_token ADD meeting_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE join_token ADD CONSTRAINT FK_D675D29667433D9C FOREIGN KEY (meeting_id) REFERENCES meeting (id)');
        $this->addSql('CREATE INDEX IDX_D675D29667433D9C ON join_token (meeting_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE join_token DROP FOREIGN KEY FK_D675D29667433D9C');
        $this->addSql('DROP INDEX IDX_D675D29667433D9C ON join_token');
        $this->addSql('ALTER TABLE join_token DROP meeting_id');
    }
}
