<?php

namespace App\Repository;

use App\Entity\Orgaunit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Orgaunit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orgaunit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orgaunit[]    findAll()
 * @method Orgaunit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrgaunitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orgaunit::class);
    }

    // /**
    //  * @return Orgaunit[] Returns an array of Orgaunit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Orgaunit
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
