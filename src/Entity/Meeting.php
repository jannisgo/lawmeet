<?php

namespace App\Entity;

use App\Repository\MeetingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MeetingRepository::class)
 */
class Meeting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $meeting_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $meeting_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mod_pass;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_pass;

    /**
     * @ORM\ManyToOne(targetEntity=Orgaunit::class, inversedBy="meetings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $host;

    /**
     * @ORM\OneToMany(targetEntity=JoinToken::class, mappedBy="meeting")
     */
    private $joinTokens;

    /**
     * @ORM\Column(type="boolean")
     */
    private $currentlyActive;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->joinTokens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMeetingId(): ?string
    {
        return $this->meeting_id;
    }

    public function setMeetingId(string $meeting_id): self
    {
        $this->meeting_id = $meeting_id;

        return $this;
    }

    public function getMeetingName(): ?string
    {
        return $this->meeting_name;
    }

    public function setMeetingName(string $meeting_name): self
    {
        $this->meeting_name = $meeting_name;

        return $this;
    }

    public function getModPass(): ?string
    {
        return $this->mod_pass;
    }

    public function setModPass(string $mod_pass): self
    {
        $this->mod_pass = $mod_pass;

        return $this;
    }

    public function getUserPass(): ?string
    {
        return $this->user_pass;
    }

    public function setUserPass(string $user_pass): self
    {
        $this->user_pass = $user_pass;

        return $this;
    }

    public function getHost(): ?Orgaunit
    {
        return $this->host;
    }

    public function setHost(?Orgaunit $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return Collection|JoinToken[]
     */
    public function getJoinTokens(): Collection
    {
        return $this->joinTokens;
    }

    public function addJoinToken(JoinToken $joinToken): self
    {
        if (!$this->joinTokens->contains($joinToken)) {
            $this->joinTokens[] = $joinToken;
            $joinToken->setMeeting($this);
        }

        return $this;
    }

    public function removeJoinToken(JoinToken $joinToken): self
    {
        if ($this->joinTokens->removeElement($joinToken)) {
            // set the owning side to null (unless already changed)
            if ($joinToken->getMeeting() === $this) {
                $joinToken->setMeeting(null);
            }
        }

        return $this;
    }

    public function getCurrentlyActive(): ?bool
    {
        return $this->currentlyActive;
    }

    public function setCurrentlyActive(bool $currentlyActive): self
    {
        $this->currentlyActive = $currentlyActive;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
