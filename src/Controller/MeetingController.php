<?php


namespace App\Controller;


use App\Entity\JoinToken;
use App\Entity\Meeting;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Doctrine\ORM\Query\AST\Join;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Validator\Constraints\Uuid;

/**
 * @Route("/meetings")
 */
class MeetingController extends AbstractController
{
    /**
     * @Route("/")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    function list(){
        $meetings = $this->getDoctrine()->getRepository(Meeting::class)->findBy([
            "host" => $this->getUser()->getOrgaunit()
        ]);

        return $this->render('meetings/list.html.twig', [
            "meetings" => $meetings,
            "title" => "Meeting-Übersicht"
        ]);
    }

    /**
     * @Route("/delete/{id}")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    function delete($id){
        $meeting = $this->getDoctrine()->getRepository(Meeting::class)->find($id);
        if($this->getUser()->getOrgaUnit() == $meeting->getHost()){
            if($meeting){
                if(sizeof($meeting->getJoinTokens()) != 0){
                    $this->addFlash("error", "Ein Fehler ist aufgetreten!|Es bestehen noch JoinTokens. Bitte löschen Sie diese, bevor Sie das Meeting löschen!");
                    return $this->redirectToRoute('app_meeting_list');
                }
                $em = $this->getDoctrine()->getManager();
                $em->remove($meeting);
                $em->flush();
                return $this->redirectToRoute('app_meeting_list');
            }else{
                return $this->redirectToRoute('app_meeting_list');
            }
        }else{
            return $this->redirectToRoute('app_meeting_list');
        }
    }

    /**
     * @Route("/detail/{id}")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    function detail($id){
        $meeting = $this->getDoctrine()->getRepository(Meeting::class)->find($id);
        if($this->getUser()->getOrgaUnit() == $meeting->getHost()){
            if($meeting){
                return $this->render('meetings/info.html.twig', [
                    "meeting" => $meeting,
                    "title" => "Detail-Infos"
                ]);
            }else{
                return $this->redirectToRoute('app_meeting_list');
            }
        }else{
            return $this->redirectToRoute('app_meeting_list');
        }
    }

    /**
     * @Route("/start/{id}")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    function startMeeting($id){
        $meeting = $this->getDoctrine()->getRepository(Meeting::class)->find($id);
        if($this->getUser()->getOrgaUnit() == $meeting->getHost()){
            if($meeting){
                $bbb = new BigBlueButton();

                $createParams = new CreateMeetingParameters($meeting->getMeetingId(), $meeting->getMeetingName() . " | " . $meeting->getHost()->getName());
                $createParams->setModeratorPassword($meeting->getModPass());
                $createParams->setAttendeePassword($meeting->getUserPass());
                $createParams->setLogoutUrl($meeting->getHost()->getWebsite());
                $createParams->setEndCallbackUrl($this->generateUrl('app_api_meetingcallback_callbackurl', ['event' => "meetingEnded", "meetingId" => $meeting->getMeetingId()]));

                $bbb->createMeeting($createParams);

                $joinMeetingParams = new JoinMeetingParameters($meeting->getMeetingId(), $this->getUser()->getRealname(), $meeting->getModPass());
                $joinMeetingParams->setRedirect(true);

                $meeting->setCurrentlyActive(true);
                $this->getDoctrine()->getManager()->persist($meeting);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirect($bbb->getJoinMeetingURL($joinMeetingParams));

            }else{
                return $this->render('meetings/notfound.html.twig');
            }
        }else{
            return $this->render('meetings/notfound.html.twig');
        }
    }

    /**
     * @Route("/join/{token}")
     */
    function joinMeetingAttendee($token){
        $jointoken = $this->getDoctrine()->getRepository(JoinToken::class)->findOneBy([
            "token" => $token
        ]);

        $bbb = new BigBlueButton();
        $meeting = $jointoken->getMeeting();
        $joinMeetingParams = new JoinMeetingParameters($meeting->getMeetingId(), $jointoken->getRealname(), $meeting->getUserPass());
        $joinMeetingParams->setRedirect(true);
        return $this->redirect($bbb->getJoinMeetingURL($joinMeetingParams));
    }

    /**
     * @Route("/create")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    function createMeeting(Request $request){
        $createmeeting = new Meeting();
        $form = $this->createForm('App\Form\CreateMeetingType', $createmeeting);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $createmeeting->setMeetingId(uniqid());
            $createmeeting->setModPass($this->getRandomString(20));
            $createmeeting->setUserPass($this->getRandomString(20));
            $createmeeting->setHost($this->getUser()->getOrgaunit());
            $createmeeting->setCurrentlyActive(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($createmeeting);
            $em->flush();
        }

        return $this->render('meetings/create.html.twig', [
            "form" => $form->createView(),
            "title" => "Meeting erstellen"
        ]);
    }

    /**
     * @Route("/detail/{meetingid}/tokens/create")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    function createJoinToken($meetingid, Request $request, MailerInterface $mailer){
        $meeting = $this->getDoctrine()->getRepository(Meeting::class)->find($meetingid);
        if($this->getUser()->getOrgaUnit() == $meeting->getHost()) {
            if ($meeting) {
                $createjointoken = new JoinToken();

                $form = $this->createForm('App\Form\CreateJoinTokenType', $createjointoken);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $createjointoken->setMeeting($meeting);
                    $createjointoken->setToken(uniqid());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($createjointoken);
                    $em->flush();

                    $tokenmail = (new TemplatedEmail())
                        ->from('LawMeet <lawmeet@it-media-hof.de>')
                        ->to($createjointoken->getEmail())
                        ->priority(Email::PRIORITY_HIGH)
                        ->subject('Sie wurden zu einer Besprechung eingeladen!')
                        ->htmlTemplate('mails/inviteEmail.html.twig')
                        ->context([
                            "jointoken" => $createjointoken,
                            "joinURL" => $this->generateUrl('app_meeting_joinmeetingattendee', ["token" => $createjointoken->getToken()], UrlGeneratorInterface::ABSOLUTE_URL)
                        ]);

                    $mailer->send($tokenmail);
                }

                return $this->render('meetings/jointokens/create.html.twig', [
                    "form" => $form->createView(),
                    "title" => "Jointoken erstellen"
                ]);
            } else {
                return $this->render('meetings/notfound.html.twig');
            }
        }else{
            return $this->redirectToRoute('app_meeting_list');
        }
    }

    /**
     * @Route("/detail/{meetingid}/tokens/{tokenid}/delete")
     */
    function deleteJoinToken($meetingid, $tokenid, MailerInterface $mailer){
        $meeting = $this->getDoctrine()->getRepository(Meeting::class)->find($meetingid);
        if($this->getUser()->getOrgaUnit() == $meeting->getHost()) {
            if($meeting){
                $token = $this->getDoctrine()->getRepository(JoinToken::class)->find($tokenid);

                $tokenmail = (new TemplatedEmail())
                    ->from('LawMeet <lawmeet@it-media-hof.de>')
                    ->to($token->getEmail())
                    ->priority(Email::PRIORITY_HIGH)
                    ->subject('Ein Besprechungslink wurde gelöscht!')
                    ->htmlTemplate('mails/deleteInviteEmail.html.twig')
                    ->context([
                        "jointoken" => $token
                    ]);

                $mailer->send($tokenmail);

                $em = $this->getDoctrine()->getManager();
                $em->remove($token);
                $em->flush();

                return $this->redirectToRoute('app_meeting_detail', ["id" => $meetingid]);
            }else{
                return $this->render('meetings/notfound.html.twig');
            }
        }else{
            return $this->redirectToRoute('app_meeting_list');
        }
    }



    function getRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

}