<?php


namespace App\Controller\api;


use App\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MeetingCallback extends AbstractController
{
    /**
     * @Route("/api/meetingcallback")
     */
    function callbackUrl(Request $request){
        if($request->get('event') == "meetingEnded"){
            $meeting = $this->getDoctrine()->getRepository(Meeting::class)->findOneBy([
                "meeting_id" => $request->get('meetingId')
            ]);
            if($meeting){
                $meeting->setCurrentlyActive(false);
                $em = $this->getDoctrine()->getManager();
                $em->persist($meeting);
                $em->flush();
                return $this->json(["success" => true]);
            }else{
                return $this->json(["error" => "Value meetingid is missing or meeting wasn't found."]);
            }
        }
        return $this->json(["success" => false, "error" => "no event value found"]);
    }
}