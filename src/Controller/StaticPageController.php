<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StaticPageController extends AbstractController
{
    /**
     * @Route("/")
     */
    function index(){
        return $this->render('static/index.html.twig', ["title" => "Home"]);
    }
}